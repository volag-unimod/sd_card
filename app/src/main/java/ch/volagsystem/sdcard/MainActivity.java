package ch.volagsystem.sdcard;

import android.Manifest;

import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static ch.volagsystem.sdcard.SdCardStorageHelper.*;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    private final int REQUEST_CODE_ASK_PERMISSIONS = 145;
    private String DIRECTORY_NAME = "LOG";
    private String FILE_NAME = "New.txt";

    private String strAlternatPath;

    @BindView(R.id.tv_path)
    TextView mTVPath;
    @BindView(R.id.ed_path)
    EditText mEdPath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermission();

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_write_file)
    public void writeFile(View view) {

        strAlternatPath = mEdPath.getText().toString();

        if (isExternalStorageWritable()) {
            String filename = FILE_NAME;
            byte[] data = new String("data to write to file").getBytes();
            File path = getExternalSdDirs(this, DIRECTORY_NAME, strAlternatPath);

            saveFile(path.getAbsolutePath(), filename, data);

            Toast.makeText(this, "Write File: " + filename + " Path: " + path, Toast.LENGTH_LONG ).show();
        }
    }


    @OnClick(R.id.btn_write_file)
    public void createDir(View view) {

        strAlternatPath = mEdPath.getText().toString();

        if (isExternalStorageWritable()) {
            String filename = FILE_NAME;
            byte[] data = new String("data to write to file").getBytes();
            File path = getExternalSdDirs(this, DIRECTORY_NAME, strAlternatPath);

            saveFile(path.getAbsolutePath(), filename, data);

            Toast.makeText(this, "Write File: " + filename + " Path: " + path, Toast.LENGTH_LONG ).show();
        }
    }



    @OnClick(R.id.btn_show_sd_path)
    public void showPath(View view) {
        StringBuilder filesList = new StringBuilder();

        File path = getExternalSdDirs(this, DIRECTORY_NAME, strAlternatPath);

        filesList.append("PATH: " + path.getAbsolutePath() + "\n\n");
        for (final File fileEntry : path.listFiles()) {
            if (fileEntry.isDirectory()) {
                filesList.append("FOLDER:[" + fileEntry.getName() + "]\n");
                //listFilesForFolder(fileEntry);
            } else {
                filesList.append(fileEntry.getName() + "\n");
            }
        }
        mTVPath.setText(filesList);
    }

    private void requestPermission() {
        //PERMISSIONS we need to ask for UC
        String[] PERMISSIONS = { Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION };

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if(grantResults != null && grantResults.length > 0 ) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        Log.i(TAG, "Permission Granted: REQUEST_CODE_ASK_PERMISSIONS ");
                    } else {
                        // Permission Denied
                        Log.e(TAG, "Permission Denied: REQUEST_CODE_ASK_PERMISSIONS ");
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}