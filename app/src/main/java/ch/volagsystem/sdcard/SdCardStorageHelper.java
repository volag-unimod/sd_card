package ch.volagsystem.sdcard;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/*
https://developer.android.com/training/data-storage/files.html#WriteExternalStorage
*/
public class SdCardStorageHelper {

    private static String TAG = SdCardStorageHelper.class.getSimpleName();

    public static File getExternalSdDirs(Context context, String type, String alternatePath) {
        File dir;
        File[] dirs = context.getExternalFilesDirs(null);

        if (dirs != null && dirs.length >= 2 ) {
            //With SD Card like CipherLab RS31
            dir = new File(dirs[dirs.length-1].getAbsolutePath() + "/" + type);
        } else {
            if (alternatePath != null && alternatePath.length() > 0) {
                //With Parameter SD_CARD_PATH in UC. Where the device does not know the path like CipherLab RS30
                dir = new File(alternatePath + type + "/");
            } else {
                //This means dirs.lenght == 1 but is it better to check to NULL
                dir = new File(dirs[dirs.length-1].getAbsolutePath() + "/" + type);
            }
        }

        createPath(dir);

        //If the paraeter alternatePath wrong
        if(! dir.exists()){
            dir = getExternalSdDirs(context, type, "");
        }

        return dir;
    }

    public static boolean createPath(File dir){

        if (!dir.exists() ) {
            if (dir.mkdirs()){
                Log.i(TAG, "getExternalSdDirs: " + dir.getAbsolutePath());
            } else {
                Log.e(TAG, "FAILD getExternalSdDirs: " + dir.getAbsolutePath());
                return false;
            }
        } else {
            Log.i(TAG, "getExternalSdDirs: " + dir.getAbsolutePath());
        }


        return true;
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            Log.i(TAG, "SD_CARD writeable");
            return true;
        }
        Log.e(TAG, "SD_Card -NOT- writeable");
        return false;
    }

    // Save the file
    public static void saveFile(String path, String filename, byte[] data) {
        File file = new File(path, filename);

        FileOutputStream fos;

        try  {
            fos = new FileOutputStream(file);
            fos.write(data);
            fos.flush();
            fos.close();
        } catch(
            FileNotFoundException e)
        {
            Log.e(TAG, e.getMessage());
        } catch (IOException e){
            Log.e(TAG, e.getMessage());
        }
        Log.i(TAG,"File write: " + file.getAbsolutePath());
    }
}